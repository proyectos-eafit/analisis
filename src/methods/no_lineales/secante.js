/**
 * @callback func
 * @param {Number} x
 * @returns {Number}
 */

/**
 * @param {Number} xi 
 * @param {Number} tolerancia 
 * @param {func} f
 * @param {Number} x 
 * @returns {Number[]}
 */
function secante(x, xi, tolerancia, f){
    var tmp = 0;
    var m_1 = x;
    var m_2 = xi;
    var k = 0;
    var iteraciones = [];
    do{
        tmp = m_1;
        m_1 = m_1 - (f(m_1)*(m_2-m_1))/(f(m_2)-f(m_1));
        m_2 = tmp;
        k+=1;
        iteraciones.push(m_1);
    }while(Math.abs(m_2-m_1)>tolerancia);
    return iteraciones;
}

//secante(7, 5, Math.pow(10, -4), (x)=>{
    //return Math.pow(x,2)-3*x-4;
//});

export default secante;
