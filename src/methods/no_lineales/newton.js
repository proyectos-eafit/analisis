/**
 * @callback func
 * @param {Number} x
 * @returns {Number}
 */

/**
 * @param {Number} Xi 
 * @param {Number} tolerancia 
 * @param {func} f
 * @param {Number} x 
 * @param {func} fp 
 * @returns {Number[]}
 */
function newton(Xi, tolerancia, f, fp){
    var m = Xi;
    var mi = Xi;
    var k=0;
    var iteraciones = [];
    do{
        mi = m;
        m = mi - (f(mi)/fp(mi));
        k+=1;
        iteraciones.push(m);
    }while(Math.abs(m-mi)>tolerancia);
    return iteraciones;
}

//newton(0, Math.pow(10, -7), (x)=>{
//    return 1/Math.exp(x) - x;
//}, (x)=>{
//    return -1/Math.exp(x) - 1;
//});

export default newton;