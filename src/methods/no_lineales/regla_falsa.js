export function regla_falsa(a, b, tol, f){
    var i = a;
    var j = b;
    var iter = [];
    if(f(i)*f(j)>0){
        return
    }

    while(true){
        var c = (((f(j)*i)-(f(i)*j))/(f(j)-f(i)));
        var fc = f(c);
        console.log(c);
        if (Math.abs(fc)<=tol)
        {
            console.log(c);
            return
        }
        if ((f(i)*f(c))<0)
        {
            j = c; 
        }
        else
        {  
            i = c;
        }
    }
}

//regla_falsa(1, 2, 0.0001, x=>
//{ return Math.pow(x,3)+4*Math.pow(x,2)-10});