export function punto_fijo(x, tolerancia, g){
    var m = x;
    var mi = x;
    k=0;
    do{
        mi = m;
        m = g(m);
        k+=1;
        console.log(`Xi=${m}`);
        console.log(`Err=${Math.abs(m-mi)}`);
    }while(Math.abs(m-mi)>tolerancia);
    console.log(`Resultado= ${m}`);
    console.log(`k=${k}`)
}

//punto_fijo(0, Math.pow(10, -4), (x)=>{
//    return 0.4*Math.exp(Math.pow(x,2));
//});