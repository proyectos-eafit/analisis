function biseccion(a, b, tolerancia, n_max, f){
    var m1 = a;
    var m = b;
    var k = 0;
    var iter = [];
    var ERR = 10E3;
    if(f(a)*f(b) > 0){ // retornar error si no cambia de signo en el intervalo [a,b]
         console.log("La funcion no cambia de signo");
         console.log(f(a)*f(b))
    }
    while(ERR>tolerancia && k<n_max){
        m1 = m;
        m = (a+b)/2;
        if(f(a)*f(m) < 0){ // cambia de signo en [a,m]
            b=m;
        }
        if(f(m)*f(b) < 0){ // cambia de signo en [m,b]
            a=m;
        }
        k+=1;
        ERR = Math.abs(m1-m);
        iter.push(m);
    }
    return {
        iter: iter,
        err: ERR
    }
}

biseccion(0, 1, 0.01, 20, (x)=>{
    return Math.pow(x, 2)-0,5;
});

//export default biseccion;