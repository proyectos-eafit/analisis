/**
 * @callback func
 * @param {Number} x
 * @returns {Number}
 */

/**
 * @param {Number} tolerancia 
 * @param {func} f
 * @param {func} fp
 * @param {func} f2p
 * @param {Number} x 
 * @returns {Number[]}
 */
function raices_multiples(x, tolerancia, f, fp, f2p){
    var m = x;
    var mi = x;
    var k=0;
    var iteraciones = [];
    do{
        mi = m;
        m = mi - ((f(mi)*fp(mi))/(Math.pow(fp(mi),2)-f(mi)*f2p(mi)));
        k+=1;
        iteraciones.push(m);
    }while(Math.abs(m-mi)>tolerancia);
    return iteraciones;
}

//raices_multiples(2, Math.pow(10, -4), (x)=>{
//    return (x-3)*Math.pow(x-1, 2);
//}, (x)=>{
//    return Math.pow(x-1,2)+2*(x-1)*(x-3);
//},(x)=>{
//    return 4*(x-1)+2*(x-3);
//});

export default raices_multiples;