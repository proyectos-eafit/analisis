import {forward_substitution, back_substitution, create_zero_matrix} from '../utils';
import * as math from 'mathjs';

/**
 * 
 * @param {Number[]} F_1 
 * @param {Number[]} F_2 
 * @param {Number} pivot_index
 * @return {{F:Number[], x: Number}}
 */
function calculate_new_row(F_1, F_2, pivot_index){
  var x = F_2[pivot_index]/F_1[pivot_index]; //F_2[pivot_index] - x*F_1[pivot_index] = 0
  return {
    F:F_1.map((f1, i)=>Number.parseFloat((F_2[i]-x*f1).toFixed(10))),
    x:x
  };
}

/**
 * 
 * @param {Number[][]} M 
 * @param {Number[][]} N
 * @param {Number} index 
 * @return {Number[][]}
 */
function pivot(M, N, index){
    let max_index = index;
    let max = Math.abs(M[index][index]);
    for(let j=index+1;j<M.length;j++){
        if(Math.abs(M[j][index])>max){
            max = Math.abs(M[j][index]);
            max_index = j;
        }
    }
    if(max_index != index){
        let fila = M[index];
        let fila_N = N[index];
        M[index] = M[max_index];
        M[max_index] = fila;
        N[index] = N[max_index];
        N[max_index] = fila_N;
    }
    return M;
}

/**
 * 
 * @param {Number[][]} A 
 * @param {Number[]} b 
 * @returns {{L:Number[][], U:Number[][], x:Number[]}}
 */
export function lupar(A, b){
    var U = A;
    var n = A.length;
    var L = create_zero_matrix(n, n);

    for(let i=0;i<n;i++){
        U = pivot(U, L, i);

        for(let j=i+1;j<n;j++){
            console.table(L);
            console.table(U);
            let row_calculation = calculate_new_row(U[i], U[j], i);
            U[j] = row_calculation.F;
            L[j][i] = row_calculation.x;
        }
    }

    L = L.map((fila, i)=>{
      return fila.map((value, j)=>{
        if(i==j){
          return 1;
        }
        return value;
      });
    })

    var z = forward_substitution(L, b);
    var x = back_substitution(U, z);
    return {
        L:L,
        U:U,
        x: x
    };
}
