import {identity, get_column} from '../utils';
import * as math from 'mathjs';

/**
 * 
 * @param {Number[][]} A 
 * @param {Number[]} b 
 * @returns {{L:Number[][], U:Number[][], x:Number[]}}
 */
export function crout(A, b){
    var n = A.length
    var L = identity(n);
    var U = identity(n);

    //for i=1:n-1
    //    for j=i+1:n
    //        L(j,i)=(A(j,i)-dot(L(j,1:i-1),U(1:i-1,i)'))/U(i,i);
    //    end
    //    for j=i+1:n
    //        U(i,j)=(A(i,j)-dot(L(i,1:i-1),U(1:i-1,j)'))/L(i,i);
    //    end
    //end

    for(let i=0; i<n-1;i++){
        for(let j=i; j<n; j++){
            var l = L[j];
            l.pop();
            var u = get_column(U, i);
            u.pop();
            L[j][i] = (A[j][i] - math.dot(l, u));
        }
        for(let j=i; j<n; j++){
            var l = L[i];
            l.pop();
            var u = get_column(U, i);
            u.pop();
            L[j][i] = (A[j][i] - math.dot(l, u));
        }
    }

    const L = zeros.map((fila, i, L)=>{ // iterar sobre las filas
        return fila.map((value, j)=>{ // iterar sobre los números de cada fila
            const dot = fila.reduce((s,_,k)=>{// producto punto
                if(k < j){
                    return s + L[i][k] * L[j][k];
                }
                return s;
            }, 0);
            if(j < i+1){
                if(i === j){
                    L[i][j] = Math.sqrt(A[i][i] - dot);
                }
                else{
                    L[i][j] = (A[i][j] - dot) / L[j][j];
                }
            }
            else{
                L[i][j] = value;
            }
            return L[i][j];
        });
    });
}