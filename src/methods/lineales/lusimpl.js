import {forward_substitution, back_substitution, identity} from '../utils';
import * as math from 'mathjs';

/**
 * 
 * @param {Number[]} F_1 
 * @param {Number[]} F_2 
 * @param {Number} pivot_index
 * @return {{F:Number[], x: Number}}
 */
function calculate_new_row(F_1, F_2, pivot_index){
  var x = F_2[pivot_index]/F_1[pivot_index]; //F_2[pivot_index] - x*F_1[pivot_index] = 0
  return {
    F:F_1.map((f1, i)=>Number.parseFloat((F_2[i]-x*f1).toFixed(10))),
    x:x
  };
}

/**
 * 
 * @param {Number[][]} A 
 * @param {Number[]} b 
 * @returns {{L:Number[][], U:Number[][], x:Number[]}}
 */
export function lusimpl(A, b){
    var L = identity(A.length);
    var U = A;
    var n = A.length;

    for(let i=0;i<n;i++){
        for(let j=i+1;j<n;j++){
            console.table(L);
            console.table(U);
            let row_calculation = calculate_new_row(U[i], U[j], i);
            U[j] = row_calculation.F;
            L[j][i] = row_calculation.x;
        }
    }
    
    // mapping de NaN a 0
    L = L.map((fila)=>{
      return fila.map((value)=>{
        if(isNaN(value)){
          return 0;
        }
        return value;
      });
    });
    U= U.map((fila)=>{
      return fila.map((value)=>{
        if(isNaN(value)){
          return 0;
        }
        return value;
      });
    });

    var z = forward_substitution(L, b);
    var x = back_substitution(U, z);
    return {
        L:L,
        U:U,
        x: x
    };
}
