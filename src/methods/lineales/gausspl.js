import { augmented, back_substitution } from '../utils';

/**
 * 
 * @param {Number[]} F_1 
 * @param {Number[]} F_2 
 * @param {Number} pivot_index
 * @return {Number[]}
 */
function calculate_new_row(F_1, F_2, pivot_index){
  var x = F_2[pivot_index]; //F_2[pivot_index] - x*F_1[pivot_index] = 0
  return F_1.map((f1, i)=>(F_2[i]-x*f1).toFixed(10));
}

/**
 * 
 * @param {Number[]} F_1 
 * @param {Number} pivot_index 
 * @return {Number[]}
 */
function prepare_pivot_row(F_1, pivot_index){
  var pivot = F_1[pivot_index];
  return F_1.map((value)=>value/pivot);
}

/**
 * 
 * @param {Number[][]} A 
 * @param {Number[]} b 
 * @return {Number[]}
 */
export function gausspl(A, b){
  var n = A.length;
  var M = augmented(A, b);
  console.table(M);
  for(let i=0;i<n;i++){
    M[i] = prepare_pivot_row(M[i], i);
    console.table(M);
    for(let j=i+1;j<n;j++){
      M[j] = calculate_new_row(M[i], M[j], i);
    }
  }
  return back_substitution(M, b);
}

/**
A = [
  [9,3,4],
  [4,3,4],
  [1,1,1]
]

b = [7,8,3]

var x = solve(A, b);

print(x, " x "); */