import { augmented, back_substitution } from '../utils';
import * as math from 'mathjs';

/**
 * 
 * @param {Number[]} F_1 
 * @param {Number[]} F_2 
 * @param {Number} pivot_index
 * @return {Number[]}
 */
function calculate_new_row(F_1, F_2, pivot_index){
  var x = F_2[pivot_index]/F_1[pivot_index]; //F_2[pivot_index] - x*F_1[pivot_index] = 0 => x=F_2[pivot_index]/F_1[pivot_index]
  return F_1.map((f1, i)=>(F_2[i]-x*f1).toFixed(10));
}

/**
 * 
 * @param {Number[]} F_1 
 * @param {Number} pivot_index 
 * @return {Number[]}
 */
function prepare_pivot_row(F_1, pivot_index){
    var pivot = F_1[pivot_index];
    return F_1.map((value)=>value/pivot);
}

/**
 * 
 * @param {Number[][]} M 
 * @param {Number} index 
 * @return {Number[][]}
 */
function pivot(M, index){
    let max_j = index;
    let max_k = index;
    var A = M;
    let max = Math.abs(A[index][index]);
    for(let j=index;j<A.length;j++){
        for(let k=index;k<A.length;k++){
            if(Math.abs(A[j][k])>max){
                max = Math.abs(A[j][k]);
                max_j = j;
                max_k = k;
            }
        }
    }
    if(max_j != index){
        let fila = A[index];
        A[index] = A[max_j];
        A[max_j] = fila;
    }
    if(max_k != index){
        // get index column
        let column = [];
        for(let i=0; i<A.length; i++){
            column.push(A[i][index]);
        }
        // swap columns
        for(let i=0; i<A.length; i++){
            A[i][index] = A[i][max_k];
            A[i][max_k] = column[i];
        }
    }
    return A;
}

/**
 * 
 * @param {Number[][]} A 
 * @param {Number[]} b 
 * @return {Number[]}
 */
export function gausstot(A, b){
  var n = A.length;
  var M = augmented(A, b);
  console.table(M);
  for(let i=0;i<n;i++){
      M = pivot(M, i);
      M[i] = prepare_pivot_row(M[i], i);
    for(let j=i+1;j<n;j++){
      M[j] = calculate_new_row(M[i], M[j], i);
      console.table(M);
    }
  }
  M[M.length-1] = prepare_pivot_row(M[M.length-1], M.length-1);
  console.table(M);
  return back_substitution(M, b);
}