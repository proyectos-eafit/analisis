import { augmented, back_substitution } from '../utils';

/**
 * 
 * @param {Number[]} F_1 
 * @param {Number[]} F_2 
 * @param {Number} pivot_index
 * @return {Number[]}
 */
function calculate_new_row(F_1, F_2, pivot_index){
  var x = F_2[pivot_index]/F_1[pivot_index]; //F_2[pivot_index] - x*F_1[pivot_index] = 0 => x=F_2[pivot_index]/F_1[pivot_index]
  return F_1.map((f1, i)=>(F_2[i]-x*f1).toFixed(10));
}

/**
 * 
 * @param {Number[]} F_1 
 * @param {Number} pivot_index 
 * @return {Number[]}
 */
function prepare_pivot_row(F_1, pivot_index){
    var pivot = F_1[pivot_index];
    return F_1.map((value)=>value/pivot);
}

/**
 * 
 * @param {Number[][]} M 
 * @param {Number} index 
 * @return {Number[][]}
 */
function pivot(M, index){
    let max_index = index;
    let max = Math.abs(M[index][index]);
    for(let j=index+1;j<M.length;j++){
        if(Math.abs(M[j][index])>max){
            max = Math.abs(M[j][index]);
            max_index = j;
        }
    }
    if(max_index != index){
        let fila = M[index];
        M[index] = M[max_index];
        M[max_index] = fila;
    }
    return M;
}

/**
 * 
 * @param {Number[][]} A 
 * @param {Number[]} b 
 * @return {Number[]}
 */
export function gausspar(A, b){
  var n = A.length;
  var M = augmented(A, b);
  for(let i=0;i<n;i++){
      M = pivot(M, i);
      M[i] = prepare_pivot_row(M[i], i);
    for(let j=i+1;j<n;j++){
      M[j] = calculate_new_row(M[i], M[j], i);
    }
  }
  M[M.length-1] = prepare_pivot_row(M[M.length-1], M.length-1);
  return back_substitution(M, b);
}
