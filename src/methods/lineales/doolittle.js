function luDecomposition(mat, n)
{
    var lower = Array(n).fill(0).map(
           x => Array(n).fill(0));
    var upper = Array(n).fill(0).map(
           x => Array(n).fill(0));
 
    for(var i = 0; i < n; i++)
    {
        for(var k = i; k < n; k++)
        {
            var sum = 0;
            for(var j = 0; j < i; j++)
            {
                sum += (lower[i][j] * upper[j][k]);
            }
            upper[i][k] = mat[i][k] - sum;
        }

        for(var k = i; k < n; k++){
            if (i == k){
                lower[i][i] = 1;
            }  
            else{
                var sum = 0;
                for(var j = 0; j < i; j++){
                    sum += (lower[k][j] * upper[j][i]);
                }
                lower[k][i] = parseInt((mat[k][i] - sum) / upper[i][i]);
            }
        }
    }

    console.log(lower);
    console.log(upper);
}
 
function setw(noOfSpace)
{
    var s = " ";
    for(i = 0; i < noOfSpace; i++){
        s += " ";
    } 
    return s;
}
 
var mat = [ [ 2, -1, -2 ],
            [ -4, 6, 3 ],
            [ -4, -2, 8 ] ];
 
luDecomposition(mat, 3);