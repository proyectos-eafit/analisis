import * as math from 'mathjs';
import { forward_substitution, back_substitution, get_lower_triangular_matrix, get_upper_triangular_matrix, neg_matrix, add_matrix, multiply_matrix } from '../utils';

/**
 * 
 * @param {Number[][]} A 
 * @param {Number[]} b 
 * @param {Number[]} x_0 
 * @param {Number} tol 
 * @param {Number} n_max 
 */
export function jacobi(A, b, x_0, tol, n_max){
    var D = math.diag(math.diag(A));
    var L = math.add(neg_matrix(get_lower_triangular_matrix(A)),D);
    var U = math.add(neg_matrix(get_upper_triangular_matrix(A)),D);
    var T = math.multiply(math.inv(D), math.add(L,U));
    var C = math.multiply(math.inv(D), b);
    var x_prev = x_0;
    var iter = [];
    var ERR = tol*2;
    
    for(var i=0; ERR>tol && i< n_max; i++){
        var x = math.add(math.multiply(T, x_prev), C);
        ERR = math.norm(math.subtract(x_prev,x));
        x_prev = x;
        iter.push(x);
    }

    return {
        iter:i,
        x:iter,
        err: ERR
    }
}