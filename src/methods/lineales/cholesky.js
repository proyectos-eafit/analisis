import * as math from 'mathjs';
import { get_submatrix, forward_substitution, back_substitution, create_zero_matrix } from '../utils';

/**
 * 
 * @param {Array<Array<Number>>} A
 */
export function cholesky(A, b){
    var len = A.length;

    if (len != A[0].length){
        throw Error('La matriz debe ser cuadrada');
    }
    for(let i=0;i<len; i++){
        if(math.det(get_submatrix(A, 0, 0, i+1)) <= 0){
            throw Error('la matriz tiene que ser positiva');
        }
    }

    const zeros = create_zero_matrix(len,len);
    const L = zeros.map((fila, i, L)=>{ // iterar sobre las filas
        return fila.map((value, j)=>{ // iterar sobre los números de cada fila
            const dot = fila.reduce((s,_,k)=>{// producto punto
                if(k < j){
                    return s + L[i][k] * L[j][k];
                }
                return s;
            }, 0);
            if(j < i+1){
                if(i === j){
                    L[i][j] = Math.sqrt(A[i][i] - dot);
                }
                else{
                    L[i][j] = (A[i][j] - dot) / L[j][j];
                }
            }
            else{
                L[i][j] = value;
            }
            return L[i][j];
        });
    });
    var U = math.transpose(L);
    var z = forward_substitution(L, b);
    var x = back_substitution(U, z);
    return {
        L:L,
        U: U,
        x:x
    };
}

//var A = [
//    [1, 4, 0],
//    [8, 15, 3],
//    [1, 9, 2]
//]
//
//console.table(cholesky(A));