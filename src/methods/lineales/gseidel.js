import * as math from "mathjs";
import {neg_matrix, get_upper_triangular_matrix, get_lower_triangular_matrix} from '../utils';

export function gseidel(A, b, x_0, tol, n_max){
    var D = math.diag(math.diag(A));
    var L = neg_matrix(math.add(get_lower_triangular_matrix(A),D));
    var U = neg_matrix(math.add(get_upper_triangular_matrix(A),D));
    var T = math.multiply(math.inv(math.subtract(D,L)), U);
    var C = math.multiply(math.inv(math.subtract(D,L)), b);
    var x_prev = x_0;
    var ERR = 10E3;
    var cont = 0;

    while(ERR>tol && cont<n_max){
        var x = math.add(math.multiply(T, x_prev), C);
        ERR = math.norm(math.subtract(x_prev, x));
        x_prev = x;
        cont++;
    }

    return {
        x:x_prev,
        iter: cont,
        err:ERR
    }
}