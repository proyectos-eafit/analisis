import { conv, create_zero_matrix, polynomial_evaluation } from "../utils";
import * as math from 'mathjs';

/**
 * 
 * @param {Number[]} X 
 * @param {Number[]} Y 
 * @return {{L:Number[][], coef:Number[]}}
 */
export function lagrange(X, Y){
    var n = X.length;
    var L = create_zero_matrix(n,n);

    for(let i=0; i<n; i++){
        var aux_0 = math.setDifference(X, [X[i]]);
        var aux = [1, -aux_0[0]];
        for(let j=1; j<n-1; j++){
            aux = conv(aux, [1, -aux_0[j]]);
            console.log(aux);
        }
        console.log("POLYEVAL")
        L[i] = math.divide(aux,polynomial_evaluation(aux, [X[i]]));
        console.table(L);
    }

    return {
        coef: math.multiply(Y, L),
        L: L
    }
}