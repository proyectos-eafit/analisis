import * as math from "mathjs";
import { create_zero_matrix, get_column, get_submatrix } from "../utils";

/**
 * 
 * @param {Number[]} X 
 * @param {Number[]} Y 
 * @return {{coef:Number}}
 */
export function difdivididas(X, Y){
    var n = X.length;
    var D = create_zero_matrix(n,n);
    
    D = replace_column(D, Y, 0);

    for(let i=1; i<n; i++){
        var aux_0 = get_column(D, i-1);
        aux_0 = aux_0.filter((_, j)=>j>=i-1 && j<=n);
        var aux = diff(aux_0);
        var aux_2 = math.subtract(X.filter((_, j)=>j>=i), X.filter((_,j)=> j<=n-i-1));
        var div = divide(aux, aux_2);
        div = [...Array(n-div.length).fill(0), ...div];
        D = replace_column(D, div, i);
    }
    
    return {
        coef: math.diag(D)
    }
}

function replace_column(M, C, i){
    var result = M;
    for(let j=0; j<result.length; j++){
        result[j][i] = C[j];
    }
    return result;
}

/**
 * 
 * @param {Number[]} A 
 * @return {Number[]}
 */
function diff(A){
    var result = Array(A.length-1);
    for(let i=0; i<result.length; i++){
        result[i] = A[i+1]-A[i];
    }
    return result;
}

function divide(A, B){
    var result = Array(A.length);

    for(let i=0; i<result.length; i++){
        result[i] = A[i]/B[i];
    }
    return result;
}