import { create_zero_matrix, get_column} from '../utils';
import * as math from 'mathjs';

/**
 * 
 * @param {Number[][]} X 
 * @param {Number[][]} Y 
 */
export function vandermonde(X, Y){
    var n = X.length;
    var A = create_zero_matrix(n, n);

    for(let i=0; i<n; i++){
        var calculate = X.map((value)=>{
            return Math.pow(value, n-i);
        });
        for(let j=0; j<n; j++){
            A[j][i] = calculate[j];
        }
    }
    console.table(A);
    console.table(Y);

    return {
        coef: math.multiply(A, math.inv(Y))
    }
}