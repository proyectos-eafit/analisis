
/**
 * 
 * @param {Array<Array<Number>>} matrix 
 * @param {Number} i 
 * @param {Number} j 
 * @param {Number} len 
 * @returns {Array<Array<Number>>};
 */
export function  get_submatrix(matrix, i, j, len){
    return matrix.slice(i, i+len).map(row => row.slice(j, j+len));
}

/**
 * 
 * @param {Number} aij
 * @param {Array<Array<Number>>} matrix
 * @param {Number} i
 * @param {Number} j
 * @return {Number}
 */
export function delta(aij, matrix, i, j) {
    var sum = aij;
	for (var k=0; k<j; ++k){
        if (matrix[i][k]){
            sum -= matrix[i][k] * matrix[j][k];
        }
    }
	return sum
}

/**
 * 
 * @param {Number} i 
 * @param {Number} j 
 * @returns {Number[][]}
 */
export function create_zero_matrix(i, j){
    return Array.from({
        length: j
      }, () => new Array(i).fill(0));
}

/**
 * 
 * @param {Array<Array<Number>>} L
 * @param {Array<Number>} b
 * @return {Array<Number>} 
 */
export function forward_substitution(L, b){
    var n=L.length;
    var y= new Array(b.length).fill(0);

    y[0] = b[0]/L[0][0];
    for(let i=1; i<n; i++){
        var dot = 0;
        for(let j=0;j<i; j++){
            dot += L[i][j]*y[j];
        }
        y[i]= (b[i] - dot)/ L[i][i];
    }
    return y;
}

/**
 * 
 * @param {Array<Array<Number>>} U
 * @param {Array<Number>} y
 * @return {Array<Number>} 
 */
export function back_substitution(U, y){
    var n=U.length;
    var x= new Array(y.length).fill(0);

    x[n-1] = y[n-1]/U[n-1][n-1];
    for(let i=n-2; i>=0; i--){
        var dot = 0;
        for(let j=i;j<n; j++){
            dot += U[i][j]*x[j];
        }
        x[i]= (y[i] - dot)/ U[i][i];
    }
    return x;
}

export function identity(n){
    let I = [];

    let i, j;
    for (i = 0; i < n; i++) {
        let r = [];
        for (j = 0; j < n; j++) {
            r.push((i == j)? 1: 0);
        }
        I.push(r)
    }
    return I;
}

/**
 * 
 * @param {Number[][]} A 
 * @return {Number[][]}
 */
export function get_upper_triangular_matrix(A){
    return A.map((fila, i)=>{
        return fila.map((value, j)=>{
            if(j>=i){
                return value;
            }
            return 0;
        });
    });
}

/**
 * 
 * @param {Number[][]} A 
 * @return {Number[][]}
 */
export function get_lower_triangular_matrix(A){
    return A.map((fila, i)=>{
        return fila.map((value, j)=>{
            if(j<=i){
                return value;
            }
            return 0;
        });
    });
}

/**
 * 
 * @param {Number[][]} A 
 * @return {Number[][]}
 */
export function neg_matrix(A){
    return A.map((fila)=>{
        return fila.map((value)=> -value);
    });
}

/**
 * 
 * @param {Number[][]} A 
 * @param {Number[][]} B 
 * @return {Number[][]}
 */
export function add_matrix(A, B){
    return A.map((fila, i)=>{
        return fila.map((value, j)=> value+B[i][j]);
    });
}

/**
 * 
 * @param {Number[][]} A 
 * @param {Number[][]} B 
 * @return {Number[][]}
 */
export function multiply_matrix(A,B){
    var result = create_zero_matrix(A.length, A.length);

    for(let i=0; i<A.length; i++){
        for(let j=0; j<A.length;j++){
            for(let k=0; k<A.length; k++){
                result[i][j]+=A[i][k]*B[k][j];
            }
        }
    }
    return result;
}

/**
 * 
 * @param {Number[][]} A 
 * @param {Number[]} b
 * @return {Number[][]} 
 */
export function augmented(A, b){
    return A.map((fila, i)=>{
        fila.push(b[i]);
        return fila;
    });
}

/**
 * 
 * @param {Number[][]} A 
 * @param {Number} index 
 * @return {Number[]}
 */
export function get_column(A, index){
    let column = [];
    for(let i=0; i<A.length; i++){
        column.push(A[i][index]);
    }
    return column;
}


/**
 * 
 * @param {Number[]} p 
 * @param {Number[]} x 
 * @return {Number[] | Number}
 */
export function polynomial_evaluation(p, x){
    console.log(p)
    console.log(x)
    var n = p.length;
    var result = Array(n).fill(0);
    for(let i=0; i<n ;i++){
        for(let j=0; j<n; j++){
            result[i] += Math.pow(x[i], n-j-1)*p[j];
        }
    };
    for(let i=0; i<n; i++){
        if(isNaN(result[i])){
            return result[0];
        }
    }
    return result;
}

/**
 * https://gist.github.com/PhotonEE/7671999
 * @param {Number[]} vec1 
 * @param {Number[]} vec2 
 * @returns {Number[]}
 */
export function conv(vec1, vec2){
    var disp = 0; // displacement given after each vector multiplication by element of another vector
    var convVec = [];
    // for first multiplication
    for (let j = 0; j < vec2.length ; j++){
        convVec.push(vec1[0] * vec2[j]);
    }
    disp = disp + 1;
    for (let i = 1; i < vec1.length ; i++){
        for (let j = 0; j < vec2.length ; j++){
            if ((disp + j) !== convVec.length){
                convVec[disp + j] = convVec[disp + j] + (vec1[i] * vec2[j])
            }
            else{
                convVec.push(vec1[i] * vec2[j]);
            }
        }
        disp = disp + 1;
    }
    return convVec;
}